﻿; MIT License
;
; Copyright © 2021 brzrkr
;
; Permission is hereby granted, free of charge, to any person obtaining a copy
; of this software and associated documentation files (the "Software"), to deal
; in the Software without restriction, including without limitation the rights
; to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
; copies of the Software, and to permit persons to whom the Software is
; furnished to do so, subject to the following conditions:
;
; The above copyright notice and this permission notice shall be included in all
; copies or substantial portions of the Software.
;
; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
; IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
; AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
; OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
; SOFTWARE.

#NoEnv
;#Warn
#KeyHistory 0
SendMode Input
SetWorkingDir %A_ScriptDir%
ListLines Off

DeadKey(sym)
{
    static TildeKeys := {Lx: "Ɫ"
    , a: "ã", Ax: "Ã"
    , n: "ñ", Nx: "Ñ"
    , o: "õ", Ox: "Õ"
    , i: "ĩ", Ix: "Ĩ"
    , u: "ũ", Ux: "Ũ"
    , e: "ẽ", Ex: "Ẽ"
    , v: "ṽ", Vx: "Ṽ"
    , y: "ỹ", Yx: "Ỹ"
    , b: "ᵬ"
    , d: "ᵭ"
    , f: "ᵮ"
    , m: "ᵯ"
    , p: "ᵱ"
    , r: "ᵲ"
    , s: "ᵴ"
    , t: "ᵵ"
    , z: "ᵶ"}
    static MacronKeys := {a: "ā", Ax: "Ā"
    , e: "ē", Ex: "Ē"
    , Ix: "Ī", i: "ī"
    , Ox: "Ō", o: "ō"
    , Ux: "Ū", u: "ū"
    , Æ: "Ǣ", æ: "ǣ"
    , Yx: "Ȳ", y: "ȳ"
    , Gx: "Ḡ", g: "ḡ"}
    static CircumflexKeys := {a: "â", Ax: "Â"
    , e: "ê", Ex: "Ê"
    , i: "î", Ix: "Î"
    , o: "ô", Ox: "Ô"
    , u: "û", Ux: "Û"
    , g: "ĝ", Gx: "Ĝ"
    , c: "ĉ", Cx: "Ĉ"
    , h: "ĥ", Hx: "Ĥ"
    , j: "ĵ", Jx: "Ĵ"
    , s: "ŝ", Sx: "Ŝ"
    , w: "ŵ", Wx: "Ŵ"
    , y: "ŷ", Yx: "Ŷ"}
    static GraveKeys := {a: "à", Ax: "À"
    , e: "è", Ex: "È"
    , i: "ì", Ix: "Ì"
    , o: "ò", Ox: "Ò"
    , u: "ù", Ux: "Ù"
    , n: "ǹ", Nx: "Ǹ"
    , y: "ỳ", Yx: "Ỳ"
    , t: "ț", Tx: "Ț"
    , s: "ș", Sx: "Ș"}
    static AcuteKeys := {a: "á", Ax: "Á"
    , e: "é", Ex: "É"
    , i: "í", Ix: "Í"
    , o: "ó", Ox: "Ó"
    , u: "ú", Ux: "Ú"
    , y: "ý", Yx: "Ý"
    , c: "ć", Cx: "Ć"
    , l: "ĺ", Lx: "Ĺ"
    , n: "ń", Nx: "Ń"
    , r: "ŕ", Rx: "Ŕ"
    , s: "ś", Sx: "Ś"
    , z: "ź", Zx: "Ź"
    , g: "ǵ", Gx: "Ǵ"
    , æ: "ǽ", Æx: "Ǽ"}
    static UmlautKeys := {"-": "⸚"
    , a: "ä", Ax: "Ä"
    , e: "ë", Ex: "Ë"
    , i: "ï", Ix: "Ï"
    , o: "ö", Ox: "Ö"
    , u: "ü", Ux: "Ü"
    , y: "ÿ", Yx: "Ÿ"
    , h: "ḧ", Hx: "Ḧ"
    , w: "ẅ", Wx: "Ẅ"
    , x: "ẍ", Xx: "Ẍ"
    , t: "ẗ"}
    static StrokeKeys := {b: "ƀ"
    , a: "ⱥ", Ax: "Ⱥ"
    , o: "ø", Ox: "Ø"
    , d: "đ", Dx: "Đ"
    , h: "ħ", Hx: "Ħ"
    , l: "ł", Lx: "Ł"
    , t: "ŧ", Tx: "Ŧ"
    , i: "ɨ", Ix: "Ɨ"
    , z: "ƶ", Zx: "Ƶ"
    , g: "ǥ", Gx: "Ǥ"
    , c: "ȼ", Cx: "Ȼ"
    , e: "ɇ", Ex: "Ɇ"
    , j: "ɉ", Jx: "Ɉ"
    , r: "ɍ", Rx: "Ɍ"
    , y: "ɏ", Yx: "Ɏ"
    , þ: "ꝥ", Þx: "Ꝥ"
    , p: "ꝑ", Px: "Ᵽ"
    , q: "ꝗ", Qx: "Ꝗ"
    , v: "ꝟ", Vx: "Ꝟ"
    , n: "ꞥ", Nx: "Ꞥ"
    , k: "ꝁ", Kx: "Ꝁ"}
    static RingKeys := {a: "å", Ax: "Å"
    , u: "ů", Ux: "Ů"
    , w: "ẘ", y: "ẙ"}
    static BreveKeys := {a: "ă", Ax: "Ă"
    , e: "ĕ", Ex: "Ĕ"
    , g: "ğ", Gx: "Ğ"
    , i: "ĭ", Ix: "Ĭ"
    , o: "ŏ", Ox: "Ŏ"
    , u: "ŭ", Ux: "Ŭ"}
    static OgonekKeys := {d: "ɖ"
    , a: "ą", Ax: "Ą"
    , e: "ę", Ex: "Ę"
    , i: "į", Ix: "Į"
    , u: "ų", Ux: "Ų"
    , o: "ǫ", Ox: "Ǫ"
    , g: "ģ", Gx: "Ģ"
    , k: "ķ", Kx: "Ķ"
    , l: "ļ", Lx: "Ļ"
    , n: "ņ", Nx: "Ņ"
    , r: "ŗ", Rx: "Ŗ"
    , s: "ş", Sx: "Ş"
    , t: "ţ", Tx: "Ţ"
    , h: "ḩ", Hx: "Ḩ"
    , c: "ç", Cx: "Ç"
    , z: "ȥ", Zx: "Ȥ"}
    static CaronKeys := {j: "ǰ"
    , c: "č", Cx: "Č"
    , d: "ď", Dx: "Ď"
    , e: "ě", Ex: "Ě"
    , l: "ľ", Lx: "Ľ"
    , n: "ň", Nx: "Ň"
    , r: "ř", Rx: "Ř"
    , s: "š", Sx: "Š"
    , t: "ť", Tx: "Ť"
    , z: "ž", Zx: "Ž"
    , a: "ǎ", Ax: "Ǎ"
    , i: "ǐ", Ix: "Ǐ"
    , o: "ǒ", Ox: "Ǒ"
    , u: "ǔ", Ux: "Ǔ"
    , g: "ǧ", Gx: "Ǧ"
    , k: "ǩ", Kx: "Ǩ"
    , h: "ȟ", Hx: "Ȟ"
    , ʒ: "ǯ", Ʒx: "Ǯ"}

    static DeadKeys := {"¯": MacronKeys
    , "^": CircumflexKeys
    , "°": RingKeys
    , "˘": BreveKeys
    , "¸": OgonekKeys
    , "~": TildeKeys
    , "``": GraveKeys
    , "'": AcuteKeys
    , """": UmlautKeys
    , "¦": StrokeKeys
    , "ˇ": CaronKeys}

    Input, Char, L1, {Backspace}{Enter}{Tab}{Del}{Ins}{AppsKey}{Sleep}{NumpadEnter}{NumpadDel}{NumpadIns}{NumpadClear}{Browser_Back}{Browser_Forward}{Browser_Refresh}{Browser_Stop}{Browser_Search}{Browser_Favorites}{Browser_Home}{Launch_Mail}{Launch_Media}{Launch_App1}{Launch_App2}{Pause}

    If (InStr(ErrorLevel, "EndKey") != 0) {
        If (GetKeyState("Shift", "P"))
            Send {LShift up}

        If (GetKeyState("Ctrl", "P"))
            Send {LCtrl up}

        If (GetKeyState("Alt", "P"))
            Send {LAlt up}

        return
    }

    ; Hack to detect uppercase variants
    If (Char != " ") {
        Char .= (RegExMatch(Char, "[A-Z]") && DeadKeys[sym].HasKey(Char "x")) ? "x" : ""
        Send % "{Text}" (DeadKeys[sym].HasKey(Char) ? DeadKeys[sym][Char] : sym Char)
    } Else Send {Text}%sym%

    If (GetKeyState("Shift", "P"))
            Send {LShift up}

    If (GetKeyState("Ctrl", "P"))
        Send {LCtrl up}

    If (GetKeyState("Alt", "P"))
        Send {LAlt up}
}

; Number row
`::
DeadKey("``")
return

+`::
DeadKey("~")
return

>!`::
SendRaw, ¨
return

+6::
DeadKey("^")
return

>!1::
SendRaw, ¹
return

>!+1::
SendRaw, ¡
return

>!2::
SendRaw, ²
return

>!3::
SendRaw, ³
return

>!4::
SendRaw, ¤
return

>!+4::
SendRaw, £
return

>!5::
SendRaw, €
return

>!+5::
SendRaw, ¢
return

>!6::
SendRaw, §
return

>!+6::
DeadKey("ˇ")
return

>!9::
SendRaw, ‘
return

>!0::
SendRaw, ’
return

>!-::
SendRaw, ¥
return

>!+-::
DeadKey("¯")
return

>!=::
SendRaw, ≠
return

>!+=::
SendRaw, ±
return

; Letters area
>!w::
SendRaw, ł
return

>!+w::
SendRaw, Ł
return

>!e::
SendRaw, ə
return

>!i::
SendRaw, ı
return

>!+i::
SendRaw, İ
return

>!u::
SendRaw, ƿ
return

>!+u::
SendRaw, Ƿ
return

>!t::
SendRaw, þ
return

>!+t::
SendRaw, Þ
return

>!d::
SendRaw, ð
return

>!+d::
SendRaw, Ð
return

>!s::
SendRaw, ß
return

>!+s::
SendRaw, ẞ
return

>!b::
SendRaw, ₿
return

>!g::
SendRaw, ʒ
return

>!+g::
SendRaw, Ʒ
return

>!\::
SendRaw, ¬
return

>!+\::
DeadKey("¦")
return

>![::
SendRaw, «
return

>!]::
SendRaw, »
return

>!+[::
SendRaw, ‹
return

>!+]::
SendRaw, ›
return

;; ; key
>!sc027::
SendRaw, ¶
return

>!+sc027::
DeadKey("°")
return

;; ' key
vkDE::
DeadKey("'")
return

+vkDE::
DeadKey("""")
return

>!vkDE::
DeadKey("˘")
return

>!+vkDE::
DeadKey("¸")
return

;; Extra key left of Z
vkE2::
SendRaw, æ
return

+vkE2::
SendRaw, Æ
return

>!vkE2::
SendRaw, œ
return

>!+vkE2::
SendRaw, Œ
return

>!x::
SendRaw, ʃ
return

>!m::
SendRaw, µ
return

>!c::
SendRaw, ©
return

>!r::
SendRaw, ®
return

;; , key
>!vkBC::
SendRaw, –
return

;; . key
>!vkBE::
SendRaw, —
return

>!+vkBE::
SendRaw, ·
return

>!/::
SendRaw, ¿
return

>!+/::
SendRaw, ‽
return

;; numpad
>!vk6F::
SendRaw, ÷
return

>!vk6A::
SendRaw, ×
return

>!Numpad0::
SendRaw, ⁰
return

>!Numpad1::
SendRaw, ¹
return

>!Numpad2::
SendRaw, ²
return

>!Numpad3::
SendRaw, ³
return

>!Numpad4::
SendRaw, ⁴
return

>!Numpad5::
SendRaw, ⁵
return

>!Numpad6::
SendRaw, ⁶
return

>!Numpad7::
SendRaw, ⁷
return

>!Numpad8::
SendRaw, ⁸
return

>!Numpad9::
SendRaw, ⁹
return
