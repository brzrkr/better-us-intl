# Better US International
![Better US international keyboard layout](resources/layout.png)
(Symbols in red are deadkeys)


## What it is
It's a keyboard layout for Windows based on US International, expanding existing deadkeys, introducing new ones, and repositioning some symbols.

With it, you can input more symbols than you'll ever need without needing to Google them or look them up on charmap. You can write in multiple languages based on the latin alphabet without having to install and change keyboard layouts. This includes, but isn't limited to, Esperanto, Italian, French, German, Romanian, Spanish, Swedish, Norwegian, Icelandic, Polish, Catalan, Anglo-Saxon, and any crazy conlang you might want to invent now that you can type fancy letters.  
Some common IPA symbols are also supported; if you find one that you need often which isn't supported, open an issue and I'll consider adding it.

Letters with multiple or repeated diacritics are currently not supported, so languages like Hungarian or Vietnamese are not fully covered by this layout. If there's enough demand, I will look into adding support for them.


## How to use it
It is a simple AutoHotkey script, but you can run it without having AutoHotkey by downloading and using only the `.exe` version.

1. Make sure that you have a US __non__-international layout set as your software keyboard, **not** a US international one.

2. Download the appropriate exe file depending on your system and keyboard.

3. Place `better-us-intl.exe` inside `%AppData%\Microsoft\Windows\Start Menu\Programs\Startup` (Windows will automatically bring you to the right location, just paste this into the address bar of Windows Explorer). This will make it so the script is automatically run when you turn on your computer.  
Otherwise you can place the exe anywhere and put a shortcut in the above-mentioned location.

You don't need to restart: just run `better-us-intl.exe`.

If you want to customise the script first or don't trust the executable file, refer to AutoHotkey's documentation for how to handle the script.

You can probably make the script work on **Linux** by using **IronAHK**, but it's untested.


## What it does
In the image above, you can see the full layout. It is based on a 105-keys layout, but you can use it on a 104-keys layout as well; you would only be missing the <kbd>æ</kbd> key.

The layout adds several deadkeys. A deadkey is a key that doesn't immediately send a symbol. Instead, press the symbol and then a letter to combine the two (e.g. <kbd>\`</kbd> + <kbd>a</kbd> = `à`).  
To input the symbol by itself, use a space (e.g. <kbd>\~</kbd> + <kbd>` `</kbd> = `~`).  
If you input the symbol twice, it is printed twice (e.g. <kbd>"</kbd> + <kbd>"</kbd> = `""`).  
If you input the symbol and then a key that does not combine with it, both are printed (e.g. <kbd>^</kbd> + <kbd>b</kbd> = `^b`).

This causes some frequently-used keys to work slightly differently than you may be used to, particularly <kbd>'</kbd> and <kbd>"</kbd>. It may take a short while to get used to the difference.

Also **note**: some additions and changes may interfere with keyboard shortcuts in some applications. Make sure to try out the layout with frequently used applications and remap affected shortcuts.

### Known issues
- Occasionally, when typing quickly in an environment that checks your input as you type, the keyboard may start behaving as though the shift key was constantly pressed. To fix this, type any deadkey character (like <kbd>"</kbd> + <kbd>a</kbd>). If you manage to reproduce the problem, please open an issue and let me know!

### Full list of changes
If you think anything should be added or changed, open an issue and I'll look into it.

#### Deadkeys
- <kbd>\`</kbd> deadkey for: à, è, ì, ò, ù, ǹ, ỳ, ț, ș and corresponding uppercase variants.
- <kbd>\~</kbd> (<kbd>Shift</kbd> + <kbd>\`</kbd>) deadkey for: ã, ẽ, ĩ, õ, ũ, ñ, ỹ, ṽ, ᵬ, ᵭ, ᵮ, ᵯ, ᵱ, ᵲ, ᵴ, ᵵ, ᵶ, corresponding uppercase variants if available, and Ɫ.
- <kbd>^</kbd> (<kbd>Shift</kbd> + <kbd>6</kbd>) deadkey for: â, ê, î, ô, û, ĝ, ĉ, ĥ, ĵ, ŝ, ŵ, ŷ and corresponding uppercase variants.
- <kbd>ˇ</kbd> (<kbd>R. Alt</kbd> + <kbd>Shift</kbd> + <kbd>6</kbd>) deadkey for: ǰ, č, ď, ě, ľ, ň, ř, š, ť, ž, ǎ, ǐ, ǒ, ǔ, ǧ, ǩ, ȟ, ǯ and corresponding uppercase variants if available.
- <kbd>¯</kbd> (<kbd>R. Alt</kbd> + <kbd>Shift</kbd> + <kbd>-</kbd>) deadkey for: ā, ē, ī, ō, ū, ȳ, ḡ, ǣ and corresponding uppercase variants.
- <kbd>'</kbd> deadkey for: á, é, í, ó, ú, ý, ć, ĺ, ń, ŕ, ś, ź, ǵ, ǽ and corresponding uppercase variants.
- <kbd>"</kbd> deadkey for: ä, ë, ï, ö, ü, ÿ, ḧ, ẅ, ẍ, ẗ, corresponding uppercase variants if available, and ⸚ (<kbd>"</kbd> + <kbd>-</kbd>).
- <kbd>°</kbd> deadkey for: å, ů, ẘ, ẙ and corresponding uppercase variantes if available.
- <kbd>˘</kbd> (<kbd>R. Alt</kbd> + <kbd>'</kbd>) deadkey for: ă, ĕ, ğ, ĭ, ŏ, ŭ and corresponding uppercase variants.
- <kbd>˛</kbd> (<kbd>R. Alt</kbd> + <kbd>Shift</kbd> + <kbd>'</kbd>) deadkey for: ą, ę, į, ų, ǫ, ģ, ķ, ļ, ņ, ŗ, ş, ţ, ḩ, ç, ɖ, ȥ and corresponding uppercase variants if available.
- <kbd>˛</kbd> (<kbd>R. Alt</kbd> + <kbd>Shift</kbd> + <kbd>¦</kbd>) deadkey for: ⱥ, ø, đ, ħ, ł, ŧ, ɨ, ƶ, ǥ, ȼ, ɇ, ɉ, ɍ, ɏ, ꝥ, ꝑ, ꝗ, ꝟ, ꞥ, ꝁ, ƀ and corresponding uppercase variants if available.

#### Additions
(Compared to non-international US layout.)
- <kbd>R. Alt</kbd> + <kbd>Shift</kbd> + <kbd>\`</kbd> = `¨`
- <kbd>R. Alt</kbd> + any digit results in the superscript version of it (e.g. <kbd>R. Alt</kbd> + <kbd>1</kbd> = `¹`). This includes the numpad.
- <kbd>R. Alt</kbd> + <kbd>Shift</kbd> + <kbd>1</kbd> = `¡`
- <kbd>R. Alt</kbd> + <kbd>4</kbd> = `¤`
- <kbd>R. Alt</kbd> + <kbd>Shift</kbd> + <kbd>4</kbd> = `£`
- <kbd>R. Alt</kbd> + <kbd>5</kbd> = `€`
- <kbd>R. Alt</kbd> + <kbd>Shift</kbd> + <kbd>5</kbd> = `¢`
- <kbd>R. Alt</kbd> + <kbd>6</kbd> = `§`
- <kbd>R. Alt</kbd> + <kbd>9</kbd> = `‘`
- <kbd>R. Alt</kbd> + <kbd>0</kbd> = `’`
- <kbd>R. Alt</kbd> + <kbd>-</kbd> = `¥`
- <kbd>R. Alt</kbd> + <kbd>=</kbd> = `≠`
- <kbd>R. Alt</kbd> + <kbd>Shift</kbd> + <kbd>=</kbd> = `±`
- <kbd>R. Alt</kbd> + <kbd>l</kbd> = `ł` (and uppercase)
- <kbd>R. Alt</kbd> + <kbd>e</kbd> = `ə`
- <kbd>R. Alt</kbd> + <kbd>r</kbd> = `®`
- <kbd>R. Alt</kbd> + <kbd>t</kbd> = `þ` (and uppercase)
- <kbd>R. Alt</kbd> + <kbd>i</kbd> = `ı`
- <kbd>R. Alt</kbd> + <kbd>Shift</kbd> + <kbd>i</kbd> = `İ`
- <kbd>R. Alt</kbd> + <kbd>y</kbd> = `ƿ` (and uppercase)
- <kbd>R. Alt</kbd> + <kbd>s</kbd> = `ß` (and uppercase)
- <kbd>R. Alt</kbd> + <kbd>d</kbd> = `ð` (and uppercase)
- <kbd>R. Alt</kbd> + <kbd>g</kbd> = `ʒ`
- <kbd>R. Alt</kbd> + <kbd>g</kbd> = `Ʒ`
- <kbd>R. Alt</kbd> + <kbd>x</kbd> = `ʃ`
- <kbd>R. Alt</kbd> + <kbd>c</kbd> = `©`
- <kbd>R. Alt</kbd> + <kbd>b</kbd> = `₿`
- <kbd>R. Alt</kbd> + <kbd>m</kbd> = `µ`
- <kbd>R. Alt</kbd> + <kbd>/</kbd> = `¬`
- <kbd>R. Alt</kbd> + <kbd>[</kbd> = `«`
- <kbd>R. Alt</kbd> + <kbd>Shift</kbd> + <kbd>[</kbd> = `‹`
- <kbd>R. Alt</kbd> + <kbd>]</kbd> = `»`
- <kbd>R. Alt</kbd> + <kbd>Shift</kbd> + <kbd>]</kbd> = `›`
- <kbd>R. Alt</kbd> + <kbd>;</kbd> = `¶`
- <kbd>R. Alt</kbd> + <kbd>Shift</kbd> + <kbd>;</kbd> = `º`
- <kbd>R. Alt</kbd> + <kbd>,</kbd> = – (en dash)
- <kbd>R. Alt</kbd> + <kbd>.</kbd> = — (em dash)
- <kbd>R. Alt</kbd> + <kbd>Shift</kbd> + <kbd>.</kbd> = `·`
- <kbd>R. Alt</kbd> + <kbd>/</kbd> = `¿`
- <kbd>R. Alt</kbd> + <kbd>Shift</kbd> + <kbd>/</kbd> = `‽`
- <kbd>R. Alt</kbd> + <kbd>Numpad /</kbd> = `÷`
- <kbd>R. Alt</kbd> + <kbd>Numpad \*</kbd> = `×`


#### Changes
- (105-keys only:) the left <kbd>\\</kbd> has been replaced with <kbd>æ</kbd>. In combination with <kbd>R. Alt</kbd> it produces <kbd>œ</kbd>; both work as expected when combined with <kbd>Shift</kbd>
